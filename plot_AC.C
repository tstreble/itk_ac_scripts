#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <iostream>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TGaxis.h>
#include <TLine.h>
#include <TEfficiency.h>
#include <TProfile.h>
#include <TF1.h>

#include "Helpers.C"

using namespace std;

// To update
TString filein = "/squark1/strebler/Upg_TRK/SingleMu_100GeV_Run4_01.00.00_RECO/Results_DC_LorentzL0/TRKNtuple.root";
TString filein_old = "/squark1/strebler/Upg_TRK/SingleMu_100GeV_23.00.03_RECO/Results_AC_default/TRK_ntuple.root";
TString filein_old2 = "/squark1/strebler/Upg_TRK/SingleMu_100GeV_23.00.03_RECO/Results_AC_default/TRK_ntuple.root";

float plot_sinhetaloc_sizeZ_2D_LX_barrel(int layer){

  int nbins_y = 100;
  float min_y = 0.;
  float max_y = 13.;

  int nbins_x = 30;
  float min_x = 0.;
  float max_x = 30;
  if(layer>=1){
    nbins_x = 8;
    max_x = 8;
    max_y=5;
  }

  TString cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0 && pixCluster_sizeZ>0",layer);
  TString cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_sizeZ>0",layer);
  TString cut = filein.Contains("Run4") ? cut_22 : cut_21p9;

  TH2F* h = single_plot2D(filein,"TRKTree","pixCluster_sizeZ","sinh(abs(hitDeco_loceta[pixCluster_hitDeco_index]))",cut,nbins_x,min_x,max_x,nbins_y,min_y,max_y);

  h->SetStats(0);
  h->SetTitle("");


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  c->SetRightMargin(0.15);
  c->SetLogz();

  h->GetYaxis()->SetTitle("sinh(|#eta(loc)|)");
  h->GetYaxis()->SetTitleOffset(1.5);
  h->GetXaxis()->SetTitle("Size-y");
  h->GetZaxis()->SetTitle("a.u.");

  h->Draw("colz");

  TProfile* pr = h->ProfileX();
  pr->Draw("same");


  TF1* f2=new TF1("f2","[0]*(x-1)",0,max_x);
  f2->SetParameter(0,0);
  f2->SetLineColor(kGreen);
  pr->Fit(f2,"R");
  f2->Draw("same");

  ATLAS_LABEL(0.2,0.91,1);

  TString fileout=Form("sinhetaloc_sizeZ_L%i_flat_barrel_pt100",layer);
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return f2->GetParameter(0);



}





float plot_angle_sizePhi_2D_LX_barrel(int layer, float slope){

  int nbins_y = 100;
  float min_y = 0.;
  float max_y = 2.;

  int nbins_x = 10;
  float min_x = 0.;
  float max_x = 10;
  if(layer>=1){
    nbins_x = 8;
    max_x = 8;
    max_y=0.5;
  }

  TString cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0 && pixCluster_sizeZ>0",layer);
  TString cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_sizeZ>0",layer);
  TString cut = filein.Contains("Run4") ? cut_22 : cut_21p9;

  TH2F* h = single_plot2D(filein,"TRKTree","pixCluster_sizePhi","hitDeco_angle[pixCluster_hitDeco_index]",cut,nbins_x,min_x,max_x,nbins_y,min_y,max_y);

  h->SetStats(0);
  h->SetTitle("");


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  c->SetRightMargin(0.15);
  c->SetLogz();

  h->GetYaxis()->SetTitle("#phi(loc)");
  h->GetYaxis()->SetTitleOffset(1.5);
  h->GetXaxis()->SetTitle("Size-x");
  h->GetZaxis()->SetTitle("a.u.");

  h->Draw("colz");

  TProfile* pr = h->ProfileX();
  pr->Draw("same");

  TF1* f1=new TF1("f1","[0]*(x-1)",0,max_x);
  f1->SetParameter(0,slope);
  f1->SetLineColor(kRed);
  f1->Draw("same");


  TF1* f2=new TF1("f2","[0]*(x-1)",0,max_x);
  f2->SetParameter(0,0);
  f2->SetLineColor(kGreen);
  pr->Fit(f2,"R");
  f2->Draw("same");

  ATLAS_LABEL(0.2,0.91,1);

  TString fileout=Form("angle_sizePhi_L%i_flat_barrel_pt100",layer);
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return f2->GetParameter(0);



}





vector<float> plot_angle_red_Pixconstant_x_2D_LX(int region,int layer,float c0){

  int nbins_x = 100;
  float min_x = 0.;
  float max_x = 1.;
  if(region==0){
    if(layer==0) max_x = 0.47;
    else if(layer==1) max_x = 0.35;
    else if(layer==2) max_x = 0.2;
    else if(layer==3) max_x = 0.16;
    else if(layer==4) max_x = 0.14;
  }
  else if(region==1) max_x = 0.15;
  else if(region==2){
    max_x = 0.1;
    if(layer>=2) max_x = 0.02;
  }

  int nbins_y = 100;
  float min_y = -0.05;
  float max_y = 0.1;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString cut = filein.Contains("Run4") ? cut_22 : cut_21p9;

  TH2F* h = single_plot2D(filein,"TRKTree",Form("abs(hitDeco_angle[pixCluster_hitDeco_index]-%f*(pixCluster_sizePhi-2))",c0),"(pixCluster_truth_locX-(pixCluster_centroid_xphi+pixCluster_LorentzShift))/(pixCluster_omegax-0.5)",cut + "&& pixCluster_sizePhi>1 && pixCluster_omegax!=0.5",nbins_x,min_x,max_x,nbins_y,min_y,max_y);

  h->SetStats(0);
  h->SetTitle("");


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  c->SetRightMargin(0.15);
  c->SetLogz();

  h->GetXaxis()->SetTitle(Form("|#phi(loc) - %f*(Size-x -2)|",c0));
  h->GetYaxis()->SetTitleOffset(1.5);
  h->GetYaxis()->SetTitle("#Deltax/(#omega_{x}-0.5)");
  h->GetZaxis()->SetTitle("a.u.");

  h->Draw("colz");

  TProfile* pr = h->ProfileX();
  pr->Draw("same");


  TF1* f2 = new TF1("f2","[0]*x+[1]",0,max_x);
  f2->SetLineColor(kGreen);
  f2->SetParameter(0,0);
  if(region==2) f2->FixParameter(0,0);
  f2->SetParameter(1,0);
  pr->Fit(f2,"R");    
  f2->Draw("same");


  ATLAS_LABEL(0.2,0.91,1);

  TString fileout;
  if(region==0)
    fileout = Form("angle_red_Pixconstant_x_L%i_barrel_flat_pt100",layer);
  else if(region==1)
    fileout = Form("angle_red_Pixconstant_x_L%i_barrel_incl_pt100",layer);
  else if(region==2)
    fileout = Form("angle_red_Pixconstant_x_L%i_endcaps_pt100",layer);
    
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  vector<float> constant = {float(f2->GetParameter(0)),float(f2->GetParameter(1))};

  return constant;


}






vector<float> plot_sinhetaloc_red_Pixconstant_y_2D_LX(int region, int layer,float c0){

  int nbins_x = 100;
  float min_x = 0.;
  float max_x = 2.;
  if(region>=1) max_x = 0.4;
  if(region==2){
    if(layer==1) max_x = 0.7;
    else if(layer==2) max_x = 0.2;
    else if(layer==3) max_x = 0.3;
  }


  int nbins_y = 100;
  float min_y = -0.15;
  float max_y = 0.25;
  if(layer==0){ min_y=-0.25; max_y=0.35;}

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString cut = filein.Contains("Run4") ? cut_22 : cut_21p9;

  TH2F* h = single_plot2D(filein,"TRKTree",Form("abs(sinh(abs(hitDeco_loceta[pixCluster_hitDeco_index]))-%f*(pixCluster_sizeZ-2))",c0),"(pixCluster_truth_locY-pixCluster_centroid_xeta)/(pixCluster_omegay-0.5)",cut + "&& pixCluster_sizeZ>1 && pixCluster_omegay!=0.5",nbins_x,min_x,max_x,nbins_y,min_y,max_y);
  h->SetStats(0);
  h->SetTitle("");


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  c->SetRightMargin(0.15);
  c->SetLogz();

  h->GetXaxis()->SetTitle(Form("|sinh(|#eta(loc)|) - %f*(Size-y-2)|",c0));
  h->GetYaxis()->SetTitleOffset(1.5);
  h->GetYaxis()->SetTitle("#Deltay/(#omega_{y}-0.5)");
  h->GetZaxis()->SetTitle("a.u.");

  h->Draw("colz");

  TProfile* pr = h->ProfileX();
  pr->Draw("same");



  float max=1.4;
  if(layer==1) max=1.;
  if(layer>=2) max=0.7;

  TF1* f2 = new TF1("f2","[0]*x+[1]",0,max);
  f2->SetLineColor(kGreen);
  f2->SetParameter(0,0);
  f2->SetParameter(1,0);
  pr->Fit(f2,"R");    
  f2->Draw("same");

  ATLAS_LABEL(0.2,0.91,1);

 TString fileout;
  if(region==0)
    fileout = Form("sinhetaloc_red_Pixconstant_y_L%i_barrel_flat_pt100",layer);
  else if(region==1)
    fileout = Form("sinhetaloc_red_Pixconstant_y_L%i_barrel_incl_pt100",layer);
  else if(region==2)
    fileout = Form("sinhetaloc_red_Pixconstant_y_L%i_endcaps_pt100",layer);

  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  vector<float> constant = {float(f2->GetParameter(0)),float(f2->GetParameter(1))};

  return constant;

}







vector<float> plot_deltax_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins = 100;
  float min = -0.05;
  float max = 0.05;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);

  vector<TH1F*> h;
  for(unsigned int i=0;i<files.size();i++){

    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;
    h.push_back( single_plot(files[i],"TRKTree","pixCluster_truth_locX-hitDeco_measurementLocX[pixCluster_hitDeco_index]",cut + " && pixCluster_sizePhi>1",nbins,min,max) );

  }

  TLegend* leg=new TLegend(0.18,0.45,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0); 

  for(unsigned int i=0;i<h.size();i++){
    h[i]->Scale(1/h[i]->Integral());
    h[i]->SetStats(0);
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }

  float maxh = h[0]->GetMaximum();
  for(unsigned int i=1;i<h.size();i++){
    if(h[i]->GetMaximum()>maxh) maxh = h[i]->GetMaximum();
  }

  
  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);

  h[0]->SetMinimum(0);
  h[0]->SetMaximum(1.5*maxh);
  h[0]->GetYaxis()->SetTitle("a.u.");
  h[0]->GetYaxis()->SetTitleOffset(1.3);
  h[0]->GetXaxis()->SetTitle("local_{x}^{true}-local_{x}^{reco} [mm]");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_x_"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  vector<float> rms;
  for(unsigned int i=0;i<h.size();i++){
    rms.push_back(sigma_IterConvergence(h[i]).first);
    cout<<leg_entry[i]<<" RMS="<<rms[i]<<endl;
  }

  return rms;



}





vector<float> plot_deltay_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins = 100;
  float min = -0.05;
  float max = 0.05;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);

  vector<TH1F*> h;
  for(unsigned int i=0;i<files.size();i++){

    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;
    h.push_back( single_plot(files[i],"TRKTree","pixCluster_truth_locY-hitDeco_measurementLocY[pixCluster_hitDeco_index]",cut + " && pixCluster_sizeZ>1",nbins,min,max) );

  }

  TLegend* leg=new TLegend(0.18,0.5,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0);

  for(unsigned int i=0;i<h.size();i++){
    h[i]->Scale(1/h[i]->Integral());
    h[i]->SetStats(0);
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }


  float maxh = h[0]->GetMaximum();
  for(unsigned int i=1;i<h.size();i++){
    if(h[i]->GetMaximum()>maxh) maxh = h[i]->GetMaximum();
  }

  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);

  h[0]->SetMinimum(0);
  h[0]->SetMaximum(1.5*maxh);
  h[0]->GetYaxis()->SetTitle("a.u.");
  h[0]->GetYaxis()->SetTitleOffset(1.3);
  h[0]->GetXaxis()->SetTitle("local_{y}^{true}-local_{y}^{reco} [mm]");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_y_"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");


  vector<float> rms;
  for(unsigned int i=0;i<h.size();i++){
    rms.push_back(sigma_IterConvergence(h[i]).first);    
    cout<<leg_entry[i]<<" RMS="<<rms[i]<<endl;
  }

  return rms;

}




void plot_deltax_vs_eta_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins_y = 100;
  float min_y = -0.05;
  float max_y = 0.05;

  int nbins_x = 50;
  float min_x = -4.;
  float max_x = 4.;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);


  vector<TGraphErrors*> h;

  for(unsigned int i=0;i<files.size();i++){

    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;

    TH2F* h2D = single_plot2D(files[i],"TRKTree","track_eta[pixCluster_track_index]","pixCluster_truth_locX-hitDeco_measurementLocX[pixCluster_hitDeco_index]","("+cut+ ") && pixCluster_sizePhi>1",nbins_x,min_x,max_x,nbins_y,min_y,max_y);

    TGraphErrors* gr = new TGraphErrors();
    for(int j=1;j<=h2D->GetNbinsX();j++){
      TH1D* h1D = h2D->ProjectionY("",j,j);
      double x = h2D->GetXaxis()->GetBinCenter(j);
      double xerr = 0.5*h2D->GetXaxis()->GetBinWidth(j);
      double y = h1D->GetMean();
      double yerr = sigma_IterConvergence(h1D).first;   
      gr->SetPoint(j-1,x,y);
      gr->SetPointError(j-1,xerr,yerr);
    }

    h.push_back(gr);

  }

  TLegend* leg=new TLegend(0.18,0.55,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0);

  for(unsigned int i=0;i<h.size();i++){
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetFillColor(0);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }




  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  //c->SetLogy();

  h[0]->GetXaxis()->SetLimits(-4.,4.);
  h[0]->GetYaxis()->SetRangeUser(-0.025,max_y);
  h[0]->GetYaxis()->SetTitle("#Delta x [mm]");
  h[0]->GetYaxis()->SetTitleOffset(1.4);
  h[0]->GetXaxis()->SetTitle("#eta");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_x_vs_eta"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return;



}






void plot_deltay_vs_eta_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins_y = 100;
  float min_y = -0.05;
  float max_y = 0.05;

  int nbins_x = 50;
  float min_x = -4.;
  float max_x = 4.;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);

  vector<TGraphErrors*> h;

  for(unsigned int i=0;i<files.size();i++){
 
    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;

    TH2F* h2D = single_plot2D(files[i],"TRKTree","track_eta[pixCluster_track_index]","pixCluster_truth_locY-hitDeco_measurementLocY[pixCluster_hitDeco_index]","("+cut+ ") && pixCluster_sizeZ>1",nbins_x,min_x,max_x,nbins_y,min_y,max_y);

    TGraphErrors* gr = new TGraphErrors();
    for(int j=1;j<=h2D->GetNbinsX();j++){
      TH1D* h1D = h2D->ProjectionY("",j,j);
      double x = h2D->GetXaxis()->GetBinCenter(j);
      double xerr = 0.5*h2D->GetXaxis()->GetBinWidth(j);
      double y = h1D->GetMean();
      double yerr = sigma_IterConvergence(h1D).first;
      gr->SetPoint(j-1,x,y);
      gr->SetPointError(j-1,xerr,yerr);
    }

    h.push_back(gr);

  }

  TLegend* leg=new TLegend(0.18,0.55,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0);

  for(unsigned int i=0;i<h.size();i++){
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetFillColor(0);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  //c->SetLogy();

  h[0]->GetXaxis()->SetLimits(-4.,4.);
  h[0]->GetYaxis()->SetRangeUser(-0.025,max_y);
  h[0]->GetYaxis()->SetTitle("#Delta y [mm]");
  h[0]->GetYaxis()->SetTitleOffset(1.4);
  h[0]->GetXaxis()->SetTitle("#eta");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_y_vs_eta"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return;



}





void plot_pull_deltax_vs_eta_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins_y = 100;
  float min_y = -10;
  float max_y = 10;

  int nbins_x = 50;
  float min_x = -4.;
  float max_x = 4.;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);

  vector<TGraphErrors*> h;

  for(unsigned int i=0;i<files.size();i++){

    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;

    TH2F* h2D = single_plot2D(files[i],"TRKTree","track_eta[pixCluster_track_index]","(pixCluster_truth_locX-hitDeco_measurementLocX[pixCluster_hitDeco_index])/sqrt(hitDeco_measurementLocCovX[pixCluster_hitDeco_index])","("+cut+ ") && pixCluster_sizePhi>1",nbins_x,min_x,max_x,nbins_y,min_y,max_y);

    TGraphErrors* gr = new TGraphErrors();
    for(int j=1;j<=h2D->GetNbinsX();j++){
      TH1D* h1D = h2D->ProjectionY("",j,j);
      double x = h2D->GetXaxis()->GetBinCenter(j);
      double xerr = 0.5*h2D->GetXaxis()->GetBinWidth(j);
      double y = h1D->GetMean();
      double yerr = sigma_IterConvergence(h1D).first;   
      gr->SetPoint(j-1,x,y);
      gr->SetPointError(j-1,xerr,yerr);
    }

    h.push_back(gr);

  }

  TLegend* leg=new TLegend(0.18,0.55,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0);

  for(unsigned int i=0;i<h.size();i++){
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }




  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  //c->SetLogy();

  h[0]->GetXaxis()->SetLimits(-4.,4.);
  h[0]->GetYaxis()->SetRangeUser(-2,5);
  h[0]->GetYaxis()->SetTitle("#Delta x/#sigma(x)");
  h[0]->GetYaxis()->SetTitleOffset(1.4);
  h[0]->GetXaxis()->SetTitle("#eta");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_pull_x_vs_eta"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return;



}






void plot_pull_deltay_vs_eta_Nhit(int region, int layer, vector<TString> files, vector<TString> leg_entry){

  int nbins_y = 100;
  float min_y = -10;
  float max_y = 10;

  int nbins_x = 50;
  float min_x = -4.;
  float max_x = 4.;

  TString cut_21p9;
  if(region==0) // Flat barrel
    cut_21p9 = Form("pixCluster_bec==0 && pixCluster_layer==%i && pixCluster_isInclined==0",layer);
  else if(region==1) // Inclined
    cut_21p9 = Form("pixCluster_layer==%i && pixCluster_isInclined==1",2*layer-2);
  else if(region==2){ // Endcaps
    if(layer==0) cut_21p9 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else cut_21p9 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  }

  TString cut_22;
  if(region==0) // Flat barrel
    cut_22 = Form("pixCluster_bec==0 && pixCluster_layer==%i",layer);
  else if(region==1) // Inclined
    cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer-1);
  else if(region==2){ // Endcaps
    if(layer==0) cut_22 = "pixCluster_bec!=0 && pixCluster_layer==0";
    else if(layer==1) cut_22 = "pixCluster_bec!=0 && (pixCluster_layer==1 || pixCluster_layer==2)"; // L0p5 + L1
    else cut_22 = Form("pixCluster_bec!=0 && pixCluster_layer==%i",2*layer);
  }

  TString detelement;
  if(region==0) detelement = Form("L%i_barrel_flat",layer);
  else if(region==1) detelement = Form("L%i_barrel_incl",layer);
  else if(region==2) detelement = Form("L%i_endcaps",layer);

  vector<TGraphErrors*> h;

  for(unsigned int i=0;i<files.size();i++){

    TString cut = files[i].Contains("Run4") ? cut_22 : cut_21p9;

    TH2F* h2D = single_plot2D(files[i],"TRKTree","track_eta[pixCluster_track_index]","(pixCluster_truth_locY-hitDeco_measurementLocY[pixCluster_hitDeco_index])/sqrt(hitDeco_measurementLocCovY[pixCluster_hitDeco_index])","("+cut+ ") && pixCluster_sizeZ>1",nbins_x,min_x,max_x,nbins_y,min_y,max_y);

    TGraphErrors* gr = new TGraphErrors();
    for(int j=1;j<=h2D->GetNbinsX();j++){
      TH1D* h1D = h2D->ProjectionY("",j,j);
      double x = h2D->GetXaxis()->GetBinCenter(j);
      double xerr = 0.5*h2D->GetXaxis()->GetBinWidth(j);
      double y = h1D->GetMean();
      double yerr = sigma_IterConvergence(h1D).first;
      gr->SetPoint(j-1,x,y);
      gr->SetPointError(j-1,xerr,yerr);
    }

    h.push_back(gr);

  }

  TLegend* leg=new TLegend(0.18,0.55,0.5,0.8);
  leg->SetHeader("#splitline{Single muon, p_{T}=100 GeV}{"+detelement+" multi-hit clusters}");
  leg->SetBorderSize(0);
  leg->SetTextSize(0.03);
  leg->SetFillStyle(0);

  for(unsigned int i=0;i<h.size();i++){
    h[i]->SetLineColor(i+1);
    if(i>3)
      h[i]->SetLineColor(i+2);
    h[i]->SetLineWidth(2);
    h[i]->SetTitle("");
    leg->AddEntry(h[i],leg_entry[i]);
  }


  TCanvas* c=new TCanvas("c","c",650,600);
  c->SetLeftMargin(0.15);
  //c->SetLogy();

  h[0]->GetXaxis()->SetLimits(-4.,4.);
  h[0]->GetYaxis()->SetRangeUser(-2,5);
  h[0]->GetYaxis()->SetTitle("#Delta y/#sigma(y)");
  h[0]->GetYaxis()->SetTitleOffset(1.4);
  h[0]->GetXaxis()->SetTitle("#eta");

  h[0]->SetTitle("");

  h[0]->Draw();
  for(unsigned int i=1;i<h.size();i++)
    h[i]->Draw("same");

  leg->Draw("same");

  ATLAS_LABEL(0.2,0.85,1);

  TString fileout="truth_residual_pull_y_vs_eta"+detelement+"_pt100_Nhit";
  c->SaveAs("plots/"+fileout+".pdf");
  c->SaveAs("plots/"+fileout+".png");

  return;



}









void getConstants(){

  vector<float> period_constants_barrel;
  for(unsigned int i=0;i<5;i++){
    float c =plot_sinhetaloc_sizeZ_2D_LX_barrel(i);
    period_constants_barrel.push_back(c);
  }

  vector<float> period_constants_phi_barrel;
  for(unsigned int i=0;i<5;i++){
    float c =plot_angle_sizePhi_2D_LX_barrel(i,period_constants_barrel[i]);
    if(i==0) period_constants_phi_barrel.push_back(c);
    else period_constants_phi_barrel.push_back(0);
  }

  vector<vector<float> > per = {period_constants_barrel,
				{0,0,0,0,0},
				{0,0,0,0,0}};

  vector<vector<float> > per_phi = {period_constants_phi_barrel,
				    {0,0,0,0,0},
				    {0,0,0,0,0}};
  
  vector<vector<float> > dummy = {{0,0,0,0,0},
				  {0,0,0,0,0},
				  {0,0,0,0,0}};


  vector<vector<vector<float> > > constants = {per_phi,per,dummy,dummy,dummy,dummy};

  for(unsigned int reg=0; reg<3; reg++){

    for(unsigned int layer=0;layer<5;layer++){

      if(reg==1 && layer<2) continue;

      vector<float> cx = plot_angle_red_Pixconstant_x_2D_LX(reg,layer,per_phi[reg][layer]);
      vector<float> cy = plot_sinhetaloc_red_Pixconstant_y_2D_LX(reg,layer,per[reg][layer]);

      constants[2][reg][layer] = cx[0];
      constants[3][reg][layer] = cx[1];
      constants[4][reg][layer] = cy[0];
      constants[5][reg][layer] = cy[1];

    }

  }

  


  cout<<"New AC constants derived"<<endl;
  cout<<"The following can be copy-pasted into AC_txt_writer.C"<<endl;
  cout<<endl;

  for(unsigned int c=0;c<6;c++){

    if(c==0) cout<<"vector<vector<float> > per_phi = { ";
    else if(c==1) cout<<"vector<vector<float> > per = { ";
    else if(c==2) cout<<"vector<vector<float> > cx1 = { ";
    else if(c==3) cout<<"vector<vector<float> > cx2 = { ";
    else if(c==4) cout<<"vector<vector<float> > cy1 = { ";
    else if(c==5) cout<<"vector<vector<float> > cy2 = { ";

    for(unsigned int reg=0;reg<3;reg++){
      cout<<"{";
      for(unsigned int layer=0;layer<5;layer++){
	cout<<constants[c][reg][layer];
	if(layer<4) cout<<", ";
      }
      if(reg<2) cout<<"},"<<endl;
      else cout<<"} };"<<endl;
    }
    cout<<endl;

  }

}










void getErrorConstants(){

  vector<vector<float> > dummy = {{0,0,0,0,0},
				  {0,0,0,0,0},
				  {0,0,0,0,0}};

  vector<vector<vector<float> > > constants = {dummy,dummy};

  vector<TString> files = {filein_old,
			   filein};
  
  vector<TString> leg_entry = {"Old",
			       "New"};

  for(unsigned int reg=0; reg<3; reg++){

    for(unsigned int layer=0;layer<5;layer++){

      if(reg==1 && layer<2) continue;

      vector<float> x = plot_deltax_Nhit(reg,layer,files,leg_entry);
      vector<float> y = plot_deltay_Nhit(reg,layer,files,leg_entry);
      constants[0][reg][layer] = x.back();
      constants[1][reg][layer] = y.back();
    }

  }




  cout<<"New AC constants derived"<<endl;
  cout<<"The following can be copy-pasted into AC_txt_writer.C"<<endl;
  cout<<endl;


  for(unsigned int c=0;c<2;c++){

    if(c==0) cout<<"vector<vector<float> > ex = { ";
    else if(c==1) cout<<"vector<vector<float> > ey = { ";

    for(unsigned int reg=0;reg<3;reg++){
      cout<<"{";
      for(unsigned int layer=0;layer<5;layer++){
	cout<<constants[c][reg][layer];
	if(layer<4) cout<<", ";
      }
      if(reg<2) cout<<"},"<<endl;
      else cout<<"} };"<<endl;
    }
    cout<<endl;

  }

  return;

}






void plot_profile(){

 vector<TString> files = {filein_old2,
			  filein_old,
			  filein};
  
  vector<TString> leg_entry = {"23-00-01",
			       "23-00-03, constants 23-00-01",
			       "23-00-03, constants 23-00-03"};

  for(unsigned int reg=0; reg<3; reg++){

    for(unsigned int layer=0;layer<5;layer++){

      if(reg==1 && layer<2) continue;

      plot_deltax_vs_eta_Nhit(reg,layer,files,leg_entry);
      plot_deltay_vs_eta_Nhit(reg,layer,files,leg_entry);  
      //plot_pull_deltax_vs_eta_Nhit(reg,layer,files,leg_entry);
      //plot_pull_deltay_vs_eta_Nhit(reg,layer,files,leg_entry);

    }

  }

  return;

}

