import os
import sys
from CoolDBWriter import CoolDBWriter
from DataBaseSpecifier import PixelClusterErrorDB_new


# read one inputfile
def main():
    if len(sys.argv) != 2:
        raise ValueError("Specify the infile as first argument!")
    filepath = sys.argv[1]
    if not os.path.isfile(filepath):
        raise ValueError("Argument is not a valid file path!")

    cw = CoolDBWriter(
            dbSpecifier=PixelClusterErrorDB_new(),
            dbFile="PixelITkError_24_00_00.db",
            tag="PixelITkError_v4_ATLAS-P2-ITK-24-00-00",
            logOnly=False)

    string_fieldValues = "{"

    with open(filepath, 'r') as inFile:
        for row, line in enumerate(inFile):
            fieldValues = line.split()
            fieldValues[0] = str(int(fieldValues[0], 10))
            for i in range(1, 9):
                fieldValues[i] = str(round(float(fieldValues[i]),6))
            print(row, fieldValues, "created from:", line)
            string_fieldValues += "\"" + fieldValues[0] + "\":["
            for i in range(1, 8):
                string_fieldValues += fieldValues[i] + ","
            string_fieldValues += fieldValues[8] + "],"

    string_fieldValues = string_fieldValues.rstrip(',')
    string_fieldValues += "}"
    field = [string_fieldValues]

    cw.writeDBrow(0, field)


if __name__ == "__main__":
    main()
