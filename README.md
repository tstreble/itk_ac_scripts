```
cd myProject
setupATLAS
lsetup git
git clone https://gitlab.cern.ch/tstreble/itk_ac_scripts.git
cd itk_ac_scripts
mkdir plots
asetup 21.9,Athena,latest
```


# Macros to determine constants
```
root -l
.L plot_AC.C+
getConstants()
getErrorConstants()
.q
```

# Macro to write constants in txt file
```
root -l
.L AC_txt_writer.C+
fill_txt()
.q
```

# Macros to convert txt file in database file
```
# Update the tag if needed in script
python WritePixelErrorDB.py PixelITkError.txt
```

# Macro to check impact of new constants
```
root -l
.L AC_txt_writer.C+
plot_profile()
.q
```
