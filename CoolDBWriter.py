import os
import sys
import logging
from PyCool import cool


class CoolDBWriter:
    def __init__(self, dbSpecifier, dbFile, tag, logOnly=False):
        self.dbs = dbSpecifier
        if len(self.dbs.fieldNames) != len(self.dbs.fieldTypes):
            raise ValueError("Number of field types does not match the number of field names!")
        self.dbFile = dbFile
        self.tag = tag
        self.logOnly = logOnly
        self.folder = None

    def writeDBrow(self, row=0, fieldValues=[]):
        if self.folder is None:
            self.prepareDB()
        if len(self.dbs.fieldNames) != len(fieldValues):
            raise ValueError("Number of field values does not match the number of fields!")
        for i, fieldName in enumerate(self.dbs.fieldNames):
            self.data[fieldName] = fieldValues[i]
        logging.debug("Writing to DB: {0}".format(fieldValues))
        if not self.logOnly:
            self.folder.storeObject(self.dbs.iovMin, self.dbs.iovMax, self.data, row, self.tag)
            logging.debug("Stored object [{0} {1} {2} {3} {4}] in db.".format(self.dbs.iovMin, self.dbs.iovMax, self.data, row, self.tag))

    def prepareDB(self):
        logging.debug("Start storing the db.")
        # remove the old db file so that we can write the new one
        try:
            os.remove(self.dbFile)
            logging.debug("Try to remove old db file with the same name.")
        except:
            logging.debug("There was no old db file with the same name.")
            pass

        logging.debug("Creating db service.")
        # get database service and open database
        dbSvc = cool.DatabaseSvcFactory.databaseService()
        logging.debug("Created db service.")

        # database accessed via physical name
        dbString = "sqlite://;schema={0};dbname={1}".format(self.dbFile, self.dbs.dbName)
        try:
            db = dbSvc.createDatabase(dbString)
            logging.debug("Try to create the db {0}.".format(dbString))
        except Exception as e:
            logging.error("{0}: Problem creating db {1}.".format(e, dbString))
            sys.exit()
        logging.info("Created db {0}.".format(dbString))

        # setup folder
        spec = cool.RecordSpecification()
        for i in range(len(self.dbs.fieldNames)):
            spec.extend(self.dbs.fieldNames[i], self.dbs.fieldTypes[i])
        logging.debug("db records specified.")

        # folder meta-data - note for Athena this has a special meaning
        desc = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
        # create the folder - multiversion version
        # last argument is createParents - if true, automatically creates parent folders if needed
        # note this will not work if the database already exists - delete mycool.db first
        folderSpec = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, spec)
        self.folder = db.createFolder(self.dbs.folderName, folderSpec, desc, True)
        logging.debug("Created db folder.")

        # now fill in some data - create a record and fill it
        self.data = cool.Record(spec)
        logging.debug("First record filled in.")
