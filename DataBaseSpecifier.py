from PyCool import cool
st = cool.StorageType
# options for the field types are:
# st.Bool st.UChar
# st.Int16 st.Int32 st.Int64
# st.UInt16 st.UInt32 st.UInt63
# st.Float st.Double
# st.String255 st.String4k st.String64k st.String16M
# st.Blob64k st.Blob16M


class PixMapOverlayDB():
    def __init__(self, run_min=240000, run_max=250000, lb=0):
        self.dbName = "OFLP200"
        self.folderName = "/PIXEL/PixMapOverlay"
        self.fieldNames = ["moduleID", "ModuleSpecialPixelMap_Clob"]
        self.fieldTypes = [st.Int32, st.String4k]
        self.iovMin = (run_min << 32 | lb)
        self.iovMax = (run_max << 32 | lb)


class PixelClusterErrorDB():
    def __init__(self, run_min=240000, run_max=250000, lb=0):
        self.dbName = "OFLP200"
        self.folderName = "/PIXEL/PixelITkClusterErrorData"
        self.fieldNames = ["waferHash", "delta_x", "delta_error_x", "delta_y", "delta_error_y"]
        self.fieldTypes = [st.Int32, st.Float, st.Float, st.Float, st.Float]
        self.iovMin = (run_min << 32 | lb)
        self.iovMax = (run_max << 32 | lb)


class PixelClusterErrorDB_new():
    def __init__(self, run_min=240000, run_max=250000, lb=0):
        self.dbName = "OFLP200"
        self.folderName = "/PIXEL/ITkClusterError"
        self.fieldNames = ["data_array"]
        self.fieldTypes = [st.String16M]
        self.iovMin = (run_min << 32 | lb)
        self.iovMax = (run_max << 32 | lb)
